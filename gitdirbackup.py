#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2015  Andrei Karas

import sys
import subprocess
import os
import re

linesSplit = re.compile("\n")
fieldsSplit = re.compile("[ ]|\t")

def showHelp():
    print "Backup all git origins in given directory and all subdirs"
    print "Usage:"
    print "  gitdirbackup.py backupdir writefile"
    print "Example:"
    print "  gitdirbackup.py ~/gitdirs restore.sh"

def getOrigin(lines):
    for line in lines:
        fields = fieldsSplit.split(line)
        if len(fields) == 3 and fields[2] == "(fetch)" and fields[0] == "origin":
            return fields[1]
    return ""

def getRemotes(lines):
    for line in lines:
        fields = fieldsSplit.split(line)
        if len(fields) == 3 and fields[2] == "(fetch)" and fields[0] != "origin":
            yield fields

def handleGitDirectory(path):
    relativePath = path[len(rootDir):]
    curDir = os.getcwd()
    dirName = os.path.dirname(path)
    relativeDirName = os.path.dirname(relativePath)
    os.chdir(dirName)
    remotes = subprocess.check_output(["git", "remote", "-v"])
    lines = linesSplit.split(remotes)
    origin = getOrigin(lines)
    if origin == "":
        print "Can't find origin for repository: " + dirName
    else:
        w.write("mkdir -p ${{DIR}}/{0}\n".format(relativeDirName))
        w.write("cd ${{DIR}}/{0}\n".format(relativeDirName))
        w.write("git clone {0} .\n".format(origin))
        for remote in getRemotes(lines):
            w.write("git remote add {0} {1}\n".format(remote[0], remote[1]))
            w.write("git fetch {0}\n".format(remote[0]))
    os.chdir(curDir)

def scanDir(path):
    if os.path.basename(path) == ".git":
        handleGitDirectory(path)
        return

    files = os.listdir(path)
    for file1 in files:
        if file1 == "." or file1 == "..":
            continue
        newPath = path + os.path.sep + file1
        if not os.path.isfile(newPath):
            scanDir(path + os.path.sep + file1)

if len(sys.argv) != 3:
    showHelp();
    exit(1)

outputFileName = os.path.abspath(sys.argv[2])
with open(outputFileName, "w") as w:
    w.write("#!/bin/bash\n\n");
    w.write("DIR=$(pwd)\n\n")
    startDir = os.path.abspath(sys.argv[1])
    rootDir = startDir
    if rootDir[len(rootDir) - 1] != os.path.sep:
        rootDir = rootDir + os.path.sep
    scanDir(startDir)
os.chmod(outputFileName, 0755)
